import java.util.ArrayList;
import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    // Method untuk kepergian kereta
    public static void keberangkatan(TrainCar kereta, int sum) {
        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<"); // Print tetap ke samping
        kereta.printCar(); // Memanggil seluruh kucing yang ada di kereta

        String keadaanKucing = ""; // variabel untuk menyimpan kondisi kucing
        double rata2Kucing = kereta.computeTotalMassIndex() / sum; // variabel untuk menyimpan rata
        // - rata berat

        if (rata2Kucing < 18.5) {
            keadaanKucing = "underweight";
        } else if (rata2Kucing >= 18.5 && rata2Kucing < 25) {
            keadaanKucing = "normal";
        } else if (rata2Kucing >= 25 && rata2Kucing < 30) {
            keadaanKucing = "overweight";
        } else if (rata2Kucing >= 30) {
            keadaanKucing = "obese";
        }

        System.out.println("Average mass index of all cats: " + rata2Kucing);
        System.out.println("In average, the cats in the train are " + keadaanKucing);
    }

    // You can add new variables or methods in this class

    public static void main(String[] args) {
        // TODO Complete me!
        Scanner input = new Scanner(System.in);
        System.out.print("Jumlah kucing :");
        int jumlah = input.nextInt();

        TrainCar keretaKucing = null;
        String[] data; // membuat array yang menyimpan semua data data kucing

        int hitung = 0; // variabel counter kucing yang dimasukan ke kereta
        ArrayList<TrainCar> panjangKereta = new ArrayList<>(); // membuat array list yang menyimpan
        // berapa panjang kereta
        ArrayList<Integer> jumlahKucingKereta = new ArrayList<>(); // membuat array list yang
        // menyimpan jumlah kucing yang
        // masuk di kereta
        input.nextLine();

        // membuat loop untuk
        for (int x = 0; x < jumlah; x++) {
            hitung += 1;
            String cat = input.nextLine(); // menerima nama nama kucing
            data = cat.split(","); // memisahkan data kucing dengan koma
            WildCat kucingKucingan = new WildCat(data[0], Double.parseDouble(data[1]),
                    Double.parseDouble(data[2])); // membuat object yang menyimpan semua data kucing
            // yang di kereta
            keretaKucing = new TrainCar(kucingKucingan, keretaKucing); // mengisi kereta dengan
            // kucing baru

            // apabila berat kereta telah melewati 250, kereta berangkat
            if (keretaKucing.computeTotalWeight() >= THRESHOLD) {
                panjangKereta.add(keretaKucing); // menambahkan ke array list keretaKucing
                jumlahKucingKereta.add(hitung); // menambahkan ke array list jumlahKucingKereta

                keretaKucing = null; // memulai antrian kereta yang baru
                jumlah = 0; // memulai dari awal perhitungan kucing
            }
        }

        // apabila antrian kereta tidak null
        if (keretaKucing != null) {
            panjangKereta.add(keretaKucing);
            jumlahKucingKereta.add(hitung);
        }

        // looping untuk menjalankan fungsi kereta yang akan diberangkatkan
        for (int x = 0; x < panjangKereta.size(); x++) {
            keberangkatan(panjangKereta.get(x), jumlahKucingKereta.get(x));
        }
    }
}
