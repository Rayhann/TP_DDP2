public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    WildCat cat;
    TrainCar next;

    // TODO Complete me!

    public TrainCar(WildCat cat) {
        // TODO Complete me!
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        // TODO Complete me!
        this.cat = cat;
        this.next = next;
    }

    public double computeTotalWeight() {
        // TODO Complete me!
        if (next == null) {
            return cat.weight + EMPTY_WEIGHT;
        } else {
            return cat.weight + EMPTY_WEIGHT + next.computeTotalWeight();
        }
    }

    public double computeTotalMassIndex() {
        // TODO Complete me!
        if (next == null) {
            return cat.computeMassIndex();
        } else {
            return cat.computeMassIndex() + next.computeTotalMassIndex();
        }
    }

    public void printCar() {
        // TODO Complete me!
        System.out.printf("--(%s)", cat.name);
        if (next == null) {
            System.out.println("");
        } else {
            next.printCar();
        }
    }
}
