package cat;

import java.util.Random;
import java.util.Scanner;
import java.util.ArrayList;

public class Cat {

    private String nama;
    private int panjang;
    private String tipe;
    static Scanner input = new Scanner(System.in);

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getPanjang() {
        return panjang;
    }

    public void setPanjang(int panjang) {
        this.panjang = panjang;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public Cat(String nama, int panjang) {
        this.nama = nama;
        this.panjang = panjang;
        if (panjang < 45) {
            this.setTipe("A");
        } else if (panjang >= 45 && panjang < 60) {
            this.setTipe("B");
        } else {
            this.setTipe("C");
        }
    }

    public void brush() {
        System.out.println("Time to clean " + this.getNama() + "'s fur\n" + this.getNama()
        + " makes a voice: Nyaaan...\nBack to the office!\n");
    }

    public void cuddle() {
        Random random = new Random();
        int rdm = random.nextInt(4);
        if (rdm == 0) {
            System.out.println(this.getNama() + " makes a voice: Miaaaw..");
        } else if (rdm == 1) {
            System.out.println(this.getNama() + " makes a voice: Purrr..");
        } else if (rdm == 2) {
            System.out.println(this.getNama() + " makes a voice: Mwaw!");
        } else {
            System.out.println(this.getNama() + " makes a voice: Mraaawr!");
        }
        System.out.println("Back to the office!\n");
    }

    public static ArrayList<Cat> input(ArrayList<Cat> kucing) {
        System.out.print("cat: ");
        int jumlah = input.nextInt();
        input.nextLine();
        if (jumlah <= 0);
        else {
            Cat Kucing;
            System.out.println("Provide the information of cat(s):");
            String info = input.nextLine();
            String[] temp = info.split("\\s&\\d+|\\||\\,");
            int jmlh = 0;
            for (int i = 0; i < temp.length; i++) {
                if (i % 2 == 0) {
                    String nama = temp[i];
                    int panjang = Integer.parseInt(temp[i + 1]);
                    Kucing = new Cat(nama, panjang);
                    kucing.add(Kucing);
                }
            }
        }
        return kucing;
    }
}
