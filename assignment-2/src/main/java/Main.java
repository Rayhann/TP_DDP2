import java.util.Scanner;
import java.util.ArrayList;
import eagle.Eagle;
import lion.Lion;
import cat.Cat;
import parrot.Parrot;
import hamster.Hamster;
import arrangement.Arrangement;
import cage.Cage;

public class Main{
    static ArrayList<Cat> kucing = new ArrayList<Cat>();
    static ArrayList<Lion> singa = new ArrayList<Lion>();
    static ArrayList<Eagle> elang = new ArrayList<Eagle>();
    static ArrayList<Parrot> beo = new ArrayList<Parrot>();
    static ArrayList<Hamster> Hamster1 = new ArrayList<Hamster>();

    public static void main(String[] args){

        //menginput jumlah dan data binatang
        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");
        kucing = Cat.input(kucing);
        singa  = Lion.input(singa);
        elang  = Eagle.input(elang);
        beo    = Parrot.input(beo);
        Hamster1= Hamster.input(Hamster1);
        
        System.out.println("Animals has been successfully recorded!\n\n=============================================\nCage arrangement:");

        //arrange the cages
        Arrangement.urutinKucing(kucing);
        Arrangement.urutinSinga(singa);
        Arrangement.urutinElang(elang);
        Arrangement.urutinBeo(beo);
        Arrangement.urutinHamster(Hamster1);
        
        //Menghitung jumlah binatang yang ada di Javari Park 
        System.out.println("NUMBER OF ANIMALS:");
        System.out.println("cat:"    + kucing.size());
        System.out.println("lion:"   + singa.size());
        System.out.println("parrot:" + beo.size());
        System.out.println("eagle:"  + elang.size());
        System.out.println("hamster:"+ Hamster1.size());        

        //visit the animals
        System.out.println("\n=============================================");
        Scanner input = new Scanner(System.in);
        while(!kucing.isEmpty() || !singa.isEmpty() || !beo.isEmpty() || !elang.isEmpty() || !Hamster1.isEmpty()){
            System.out.println("Which animal you want to visit?");
            System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99:exit)");
            int masukan = Integer.parseInt(input.nextLine());
            boolean terdapat = false;

            if(masukan == 1){
                System.out.println("Mention the name of cat you want to visit: ");
                String namakucing = input.nextLine();
                for(int x = 0; x<kucing.size(); x++){
                    if (kucing.get(x).getNama().equals(namakucing)){
                        System.out.println("You are visiting " + namakucing + " (cat) now, what would you like to do?\n1: Brush the fur 2: Cuddle\n");
                        int action = input.nextInt();
                        input.nextLine();
                        if(action == 1){
                            kucing.get(x).brush();
                        }
                        else if(action == 2){
                            kucing.get(x).cuddle();
                        }
                        terdapat = true;
                    }
                }
                if(terdapat == false){
                    System.out.println("There is no cat with that name! Back to the office!\n");
                }
                terdapat = false;
            }

            else if(masukan == 2){
                System.out.println("Mention the name of eagle you want to visit: ");
                String namaelang = input.nextLine();
                for(int x = 0; x<elang.size(); x++){
                    if (elang.get(x).getNama().equals(namaelang)){
                        System.out.println("You are visiting " + namaelang + " (eagle) now, what would you like to do?\n1: Order to fly\n");
                        int action = input.nextInt();
                        if(action == 1){
                            elang.get(x).orderFly();
                        }
                        else if(action == 2){
                            elang.get(x).nothing();
                        }
                        terdapat = true;
                    }
                }
                if(terdapat == false){
                    System.out.println("There is no eagle with that name! Back to the office!\n");
                }
                terdapat = false;
            }
                
            else if(masukan == 3){
                System.out.println("Mention the name of hamster you want to visit: ");
                String namahamster = input.nextLine();
                for(int x = 0; x<Hamster1.size(); x++){
                    if (Hamster1.get(x).getNama().equals(namahamster)){
                        System.out.println("You are visiting " + namahamster + " (hamster) now, what would you like to do?\n1: See it gnawing 2: Order to run in the hamster wheel\n");
                        int action = input.nextInt();
                        input.nextLine();
                        if(action == 1){
                            Hamster1.get(x).gnaw();
                        }
                        else if(action == 2){
                            Hamster1.get(x).runTheWheel();
                        }
                        terdapat = true;
                    }
                }
                if(terdapat == false){
                    System.out.println("There is no hamster with that name! Back to the office!\n");
                }
                terdapat = false;
            }
                
            else if(masukan == 4){
                System.out.println("Mention the name of parrot you want to visit: ");
                String namabeo = input.nextLine();
                for(int x = 0; x<beo.size(); x++){
                    if (beo.get(x).getNama().equals(namabeo)){
                        System.out.println("You are visiting " + namabeo + " (parrot) now, what would you like to do?\n1: Order to fly 2: Do conversation\n");
                        int action = input.nextInt();
                        input.nextLine();
                        if(action == 1){
                            beo.get(x).fly();
                        }
                        else if(action == 2){
                            beo.get(x).conversation();
                        }
                        terdapat = true;
                    }
                }
                if(terdapat == false){
                    System.out.println("There is no parrot with that name! Back to the office!\n");
                }
                terdapat = false;
            }

            else if(masukan == 5){
                System.out.println("Mention the name of lion you want to visit: ");
                String namasinga = input.nextLine();
                for(int x = 0; x<singa.size(); x++){
                    if (singa.get(x).getNama().equals(namasinga)){
                        System.out.println("You are visiting " + namasinga + " (lion) now, what would you like to do?\n1: See it hunting 2: Brush the mane 3: Distrub it\n");
                        int action = input.nextInt();
                        input.nextLine();
                        if(action == 1){
                            singa.get(x).hunting();
                        }
                        else if(action == 2){
                            singa.get(x).brushMane();
                        }
                        else if(action == 3){
                            singa.get(x).disturb();
                        }
                        terdapat = true;
                    }
                }
                if(terdapat == false){
                    System.out.println("There is no lion with that name! Back to the office!\n");
                }
                terdapat = false;
            }

            else if(masukan == 99){
                System.exit(0);
            }
            else{
                System.out.println("Please enter a valid command!");
            }
        }
    }   
}