package eagle;

import java.util.Scanner;
import java.util.ArrayList;

public class Eagle {

    private String nama;
    private int panjang;
    private String tipe;
    static Scanner input = new Scanner(System.in);

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getPanjang() {
        return panjang;
    }

    public void setPanjang(int panjang) {
        this.panjang = panjang;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public Eagle(String nama, int panjang) {
        this.nama = nama;
        this.panjang = panjang;
        if (panjang < 75) {
            this.setTipe("A");
        } else if (panjang >= 75 && panjang < 90) {
            this.setTipe("B");
        } else {
            this.setTipe("C");
        }
    }

    public void orderFly() {
        System.out.println(this.getNama() + " makes a voice: kwaakk...\n");
        System.out.println("You hurt!\nBack to the office!\n");
    }

    public void nothing(){
        System.out.println("You do nothing...\nBack to the office!\n");
    }

    public static ArrayList<Eagle> input(ArrayList<Eagle> elang) {
        System.out.print("eagle: ");
        int jumlah = input.nextInt();
        input.nextLine();
        if (jumlah <= 0);
        else {
            Eagle Elang;
            System.out.println("Provide the information of eagle(s):");
            String info = input.nextLine();
            String[] temp = info.split("\\s&\\d+|\\||\\,");
            int jmlh = 0;
            for (int i = 0; i < temp.length; i++) {
                if (i % 2 == 0) {
                    String nama = temp[i];
                    int panjang = Integer.parseInt(temp[i + 1]);
                    Elang = new Eagle(nama, panjang);
                    elang.add(Elang);
                }
            }
        }
        return elang;
    }
}
