package lion;

import java.util.Scanner;
import java.util.ArrayList;

public class Lion {

    private String nama;
    private int panjang;
    private String tipe;
    static Scanner input = new Scanner(System.in);

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getPanjang() {
        return panjang;
    }

    public void setPanjang(int panjang) {
        this.panjang = panjang;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public Lion(String nama, int panjang) {
        this.nama = nama;
        this.panjang = panjang;
        if (panjang < 75) {
            this.setTipe("A");
        } else if (panjang >= 75 && panjang < 90) {
            this.setTipe("B");
        } else {
            this.setTipe("C");
        }
    }

    public void hunting() {
        System.out.println("Lion is hunting..");
        System.out.println(this.getNama() + " makes a voice: err...!\nBack to the office!\n");
    }

    public void brushMane() {
        System.out.println("Clean the lion's mane..");
        System.out.println(this.getNama() + " makes a voice: Hauhhmm!\nBack to the office!\n");
        
    }

    public void disturb(){
        System.out.println(this.getNama() + " makes a voice: HAUHHMM!\nBack to the office!\n");
    }

    public static ArrayList<Lion> input(ArrayList<Lion> singa) {
        System.out.print("lion: ");
        int jumlah = input.nextInt();
        input.nextLine();
        if (jumlah <= 0);
        else {
            Lion Singa;
            System.out.println("Provide the information of lion(s):");
            String info = input.nextLine();
            String[] temp = info.split("\\s&\\d+|\\||\\,");
            int jmlh = 0;
            for (int i = 0; i < temp.length; i++) {
                if (i % 2 == 0) {
                    String nama = temp[i];
                    int panjang = Integer.parseInt(temp[i + 1]);
                    Singa = new Lion(nama, panjang);
                    singa.add(Singa);
                }
            }
        }
        return singa;
    }
}
