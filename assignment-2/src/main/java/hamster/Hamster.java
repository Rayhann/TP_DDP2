package hamster;

import java.util.Scanner;
import java.util.ArrayList;

public class Hamster {

    private String nama;
    private int panjang;
    private String tipe;
    static Scanner input = new Scanner(System.in);

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getPanjang() {
        return panjang;
    }

    public void setPanjang(int panjang) {
        this.panjang = panjang;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public Hamster(String nama, int panjang) {
        this.nama = nama;
        this.panjang = panjang;
        if (panjang < 45) {
            this.setTipe("A");
        } else if (panjang >= 45 && panjang < 60) {
            this.setTipe("B");
        } else {
            this.setTipe("C");
        }
    }

    public void gnaw() {
        System.out.println(this.getNama() + " makes a voice: ngkkrit.. ngkkrrriiit\nBack to the office!\n");
    }

    public void runTheWheel() {
        System.out.println(this.getNama() + " makes a voice: trrr... trrr...\nBack to the office!\n");
        
    }

    public static ArrayList<Hamster> input(ArrayList<Hamster> Hamster1) {
        System.out.print("hamster: ");
        int jumlah = input.nextInt();
        input.nextLine();
        if (jumlah <= 0);
        else {
            Hamster Hamster2;
            System.out.println("Provide the information of hamster(s):");
            String info = input.nextLine();
            String[] temp = info.split("\\s&\\d+|\\||\\,");
            int jmlh = 0;
            for (int i = 0; i < temp.length; i++) {
                if (i % 2 == 0) {
                    String nama = temp[i];
                    int panjang = Integer.parseInt(temp[i + 1]);
                    Hamster2 = new Hamster(nama, panjang);
                    Hamster1.add(Hamster2);
                }
            }
        }
        return Hamster1;
    }
}
