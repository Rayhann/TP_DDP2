package parrot;

import java.util.Scanner;
import java.util.ArrayList;

public class Parrot {

    private String nama;
    private int panjang;
    private String tipe;
    static Scanner input = new Scanner(System.in);

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getPanjang() {
        return panjang;
    }

    public void setPanjang(int panjang) {
        this.panjang = panjang;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public Parrot(String nama, int panjang) {
        this.nama = nama;
        this.panjang = panjang;
        if (panjang < 45) {
            this.setTipe("A");
        } else if (panjang >= 45 && panjang < 60) {
            this.setTipe("B");
        } else {
            this.setTipe("C");
        }
    }

    public void fly() {
        System.out.println("Parrot "+ this.getNama() + " flies!\n" + this.getNama() + " makes a voice: FLYYYY.....\nBack to the office!\n");
    }

    public void conversation() {
        System.out.println("You say: ");
        String talk = input.nextLine();
        String parrotTalk = talk.toUpperCase();
        System.out.println(this.getNama() + " says: " + parrotTalk + "\n" + "Back to the office!\n");
        
    }
    public void doNothing(){
        System.out.println(this.getNama() + " says: HM?\nBack to the office!\n");
    }

    public static ArrayList<Parrot> input(ArrayList<Parrot> beo) {
        System.out.print("parrot: ");
        int jumlah = input.nextInt();
        input.nextLine();
        if (jumlah <= 0);
        else {
            Parrot Beo;
            System.out.println("Provide the information of parrot(s):");
            String info = input.nextLine();
            String[] temp = info.split("\\s&\\d+|\\||\\,");
            int jmlh = 0;
            for (int i = 0; i < temp.length; i++) {
                if (i % 2 == 0) {
                    String nama = temp[i];
                    int panjang = Integer.parseInt(temp[i + 1]);
                    Beo = new Parrot(nama, panjang);
                    beo.add(Beo);
                }
            }
        }
        return beo;
    }
}
