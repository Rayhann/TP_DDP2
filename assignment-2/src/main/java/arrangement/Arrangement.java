package arrangement;
import eagle.Eagle;
import lion.Lion;
import cat.Cat;
import parrot.Parrot;
import hamster.Hamster;
import java.util.ArrayList;
import cage.Cage;
public class Arrangement{
	private static ArrayList<String> Level_1;
	private static ArrayList<String> Level_2;
	private static ArrayList<String> Level_3;

	public static void urutinKucing(ArrayList<Cat> kucing){
		if(kucing.isEmpty());
		else {
			Cage kandang = new Cage("cat");
			Level_1 = new ArrayList<String>();
			Level_2 = new ArrayList<String>();
			Level_3 = new ArrayList<String>();
			if (kucing.size() == 2){
				Level_1.add(kucing.get(0).getNama() + "(" + kucing.get(0).getPanjang() + "-" + kucing.get(0).getTipe() + "),");
				Level_2.add(kucing.get(1).getNama() + "(" + kucing.get(1).getPanjang() + "-" + kucing.get(1).getTipe() + "),");
			}
			else if (kucing.size() == 1){
				Level_1.add(kucing.get(0).getNama() + "(" + kucing.get(0).getPanjang() + "-" + kucing.get(0).getTipe() + "),");
			}
			else {
				for(int i = kucing.size()/3*2; i < kucing.size(); i++){
					Level_3.add(kucing.get(i).getNama() + "(" + kucing.get(i).getPanjang() + "-" + kucing.get(i).getTipe() + "),");
				}
				for(int i = kucing.size()/3; i < kucing.size()/3*2; i++){
					Level_2.add(kucing.get(i).getNama() + "(" + kucing.get(i).getPanjang() + "-" + kucing.get(i).getTipe() + "),");
				}
				for (int i = 0; i < kucing.size()/3; i++){
					Level_1.add(kucing.get(i).getNama() + "(" + kucing.get(i).getPanjang() + "-" + kucing.get(i).getTipe() + "),");
				}
			}
			System.out.println("Location: " + kandang.getTipe());
			System.out.print("Level 3: ");
			for(String i:Level_3){
				System.out.print(i);
			}
			System.out.print("\nLevel 2: ");
			for(String i:Level_2){
				System.out.print(i);
			}
			System.out.print("\nLevel 1: ");
			for(String i:Level_1){
				System.out.print(i);
			}
			System.out.println("\n\nAfter rearrangment...");
			if(Level_2.size()==0 && Level_3.size()==0){
				System.out.print("Level 3: ");
				for(int i = Level_1.size()-1; i>= 0; i--){
					System.out.print(Level_1.get(i));
				}
				System.out.print("\nlevel 2: ");
				System.out.print("\nlevel 1: ");
			}
			else {
				System.out.print("level 3: ");
				for(int i=Level_2.size()-1; i >= 0; i--){
					System.out.print(Level_2.get(i));
				}
				System.out.print("\nlevel 2: ");
				for(int i=Level_1.size()-1; i >= 0; i--){
				System.out.print(Level_1.get(i));
				}
				System.out.print("\nlevel 1: ");
				for(int i=Level_3.size()-1; i >= 0; i--){
				System.out.print(Level_3.get(i));
				}
			}
			System.out.println();
		}
	}

	public static void urutinHamster(ArrayList<Hamster> hamster){
		if(hamster.isEmpty());
		else {
			Cage kandang = new Cage("hamster");
			Level_1 = new ArrayList<String>();
			Level_2 = new ArrayList<String>();
			Level_3 = new ArrayList<String>();
			if (hamster.size() == 2){
				Level_1.add(hamster.get(0).getNama() + "(" + hamster.get(0).getPanjang() + "-" + hamster.get(0).getTipe() + "),");
				Level_2.add(hamster.get(1).getNama() + "(" + hamster.get(1).getPanjang() + "-" + hamster.get(1).getTipe() + "),");
			}
			else if (hamster.size() == 1){
				Level_1.add(hamster.get(0).getNama() + "(" + hamster.get(0).getPanjang() + "-" + hamster.get(0).getTipe() + "),");
			}
			else {
				for(int i = hamster.size()/3*2; i < hamster.size(); i++){
					Level_3.add(hamster.get(i).getNama() + "(" + hamster.get(i).getPanjang() + "-" + hamster.get(i).getTipe() + "),");
				}
				for(int i = hamster.size()/3; i < hamster.size()/3*2; i++){
					Level_2.add(hamster.get(i).getNama() + "(" + hamster.get(i).getPanjang() + "-" + hamster.get(i).getTipe() + "),");
				}
				for (int i = 0; i < hamster.size()/3; i++){
					Level_1.add(hamster.get(i).getNama() + "(" + hamster.get(i).getPanjang() + "-" + hamster.get(i).getTipe() + "),");
				}
			}
			System.out.println("Location: " + kandang.getTipe());
			System.out.print("Level 3: ");
			for(String i:Level_3){
				System.out.print(i);
			}
			System.out.print("\nLevel 2: ");
			for(String i:Level_2){
				System.out.print(i);
			}
			System.out.print("\nLevel 1: ");
			for(String i:Level_1){
				System.out.print(i);
			}
			System.out.println("\n\nAfter rearrangment...");
			if(Level_2.size()==0 && Level_3.size()==0){
				System.out.print("Level 3: ");
				for(int i = Level_1.size()-1; i>= 0; i--){
					System.out.print(Level_1.get(i));
				}
				System.out.print("\nlevel 2: ");
				System.out.print("\nlevel 1: ");
			}
			else {
				System.out.print("level 3: ");
				for(int i=Level_2.size()-1; i >= 0; i--){
					System.out.print(Level_2.get(i));
				}
				System.out.print("\nlevel 2: ");
				for(int i=Level_1.size()-1; i >= 0; i--){
				System.out.print(Level_1.get(i));
				}
				System.out.print("\nlevel 1: ");
				for(int i=Level_3.size()-1; i >= 0; i--){
				System.out.print(Level_3.get(i));
				}
			}
			System.out.println();
		}
	}

	public static void urutinBeo(ArrayList<Parrot> beo){
		if(beo.isEmpty());
		else {
			Cage kandang = new Cage("parrot");
			Level_1 = new ArrayList<String>();
			Level_2 = new ArrayList<String>();
			Level_3 = new ArrayList<String>();
			if (beo.size() == 2){
				Level_1.add(beo.get(0).getNama() + "(" + beo.get(0).getPanjang() + "-" + beo.get(0).getTipe() + "),");
				Level_2.add(beo.get(1).getNama() + "(" + beo.get(1).getPanjang() + "-" + beo.get(1).getTipe() + "),");
			}
			else if (beo.size() == 1){
				Level_1.add(beo.get(0).getNama() + "(" + beo.get(0).getPanjang() + "-" + beo.get(0).getTipe() + "),");
			}
			else {
				for(int i = beo.size()/3*2; i < beo.size(); i++){
					Level_3.add(beo.get(i).getNama() + "(" + beo.get(i).getPanjang() + "-" + beo.get(i).getTipe() + "),");
				}
				for(int i = beo.size()/3; i < beo.size()/3*2; i++){
					Level_2.add(beo.get(i).getNama() + "(" + beo.get(i).getPanjang() + "-" + beo.get(i).getTipe() + "),");
				}
				for (int i = 0; i < beo.size()/3; i++){
					Level_1.add(beo.get(i).getNama() + "(" + beo.get(i).getPanjang() + "-" + beo.get(i).getTipe() + "),");
				}
			}
			System.out.println("Location: " + kandang.getTipe());
			System.out.print("Level 3: ");
			for(String i:Level_3){
				System.out.print(i);
			}
			System.out.print("\nLevel 2: ");
			for(String i:Level_2){
				System.out.print(i);
			}
			System.out.print("\nLevel 1: ");
			for(String i:Level_1){
				System.out.print(i);
			}
			System.out.println("\n\nAfter rearrangment...");
			if(Level_2.size()==0 && Level_3.size()==0){
				System.out.print("Level 3: ");
				for(int i = Level_1.size()-1; i>= 0; i--){
					System.out.print(Level_1.get(i));
				}
				System.out.print("\nlevel 2: ");
				System.out.print("\nlevel 1: ");
			}
			else {
				System.out.print("level 3: ");
				for(int i=Level_2.size()-1; i >= 0; i--){
					System.out.print(Level_2.get(i));
				}
				System.out.print("\nlevel 2: ");
				for(int i=Level_1.size()-1; i >= 0; i--){
				System.out.print(Level_1.get(i));
				}
				System.out.print("\nlevel 1: ");
				for(int i=Level_3.size()-1; i >= 0; i--){
				System.out.print(Level_3.get(i));
				}
			}
			System.out.println();
		}
	}

	public static void urutinSinga(ArrayList<Lion> singa){
		if(singa.isEmpty());
		else {
			Cage kandang = new Cage("lion");
			Level_1 = new ArrayList<String>();
			Level_2 = new ArrayList<String>();
			Level_3 = new ArrayList<String>();
			if (singa.size() == 2){
				Level_1.add(singa.get(0).getNama() + "(" + singa.get(0).getPanjang() + "-" + singa.get(0).getTipe() + "),");
				Level_2.add(singa.get(1).getNama() + "(" + singa.get(1).getPanjang() + "-" + singa.get(1).getTipe() + "),");
			}
			else if (singa.size() == 1){
				Level_1.add(singa.get(0).getNama() + "(" + singa.get(0).getPanjang() + "-" + singa.get(0).getTipe() + "),");
			}
			else {
				for(int i = singa.size()/3*2; i < singa.size(); i++){
					Level_3.add(singa.get(i).getNama() + "(" + singa.get(i).getPanjang() + "-" + singa.get(i).getTipe() + "),");
				}
				for(int i = singa.size()/3; i < singa.size()/3*2; i++){
					Level_2.add(singa.get(i).getNama() + "(" + singa.get(i).getPanjang() + "-" + singa.get(i).getTipe() + "),");
				}
				for (int i = 0; i < singa.size()/3; i++){
					Level_1.add(singa.get(i).getNama() + "(" + singa.get(i).getPanjang() + "-" + singa.get(i).getTipe() + "),");
				}
			}
			System.out.println("Location: " + kandang.getTipe());
			System.out.print("Level 3: ");
			for(String i:Level_3){
				System.out.print(i);
			}
			System.out.print("\nLevel 2: ");
			for(String i:Level_2){
				System.out.print(i);
			}
			System.out.print("\nLevel 1: ");
			for(String i:Level_1){
				System.out.print(i);
			}
			System.out.println("\n\nAfter rearrangment...");
			if(Level_2.size()==0 && Level_3.size()==0){
				System.out.print("Level 3: ");
				for(int i = Level_1.size()-1; i>= 0; i--){
					System.out.print(Level_1.get(i));
				}
				System.out.print("\nlevel 2: ");
				System.out.print("\nlevel 1: ");
			}
			else {
				System.out.print("level 3: ");
				for(int i=Level_2.size()-1; i >= 0; i--){
					System.out.print(Level_2.get(i));
				}
				System.out.print("\nlevel 2: ");
				for(int i=Level_1.size()-1; i >= 0; i--){
				System.out.print(Level_1.get(i));
				}
				System.out.print("\nlevel 1: ");
				for(int i=Level_3.size()-1; i >= 0; i--){
				System.out.print(Level_3.get(i));
				}
			}
			System.out.println();
		}
	}

	public static void urutinElang(ArrayList<Eagle> elang){
		if(elang.isEmpty());
		else {
			Cage kandang = new Cage("eagle");
			Level_1 = new ArrayList<String>();
			Level_2 = new ArrayList<String>();
			Level_3 = new ArrayList<String>();
			if (elang.size() == 2){
				Level_1.add(elang.get(0).getNama() + "(" + elang.get(0).getPanjang() + "-" + elang.get(0).getTipe() + "),");
				Level_2.add(elang.get(1).getNama() + "(" + elang.get(1).getPanjang() + "-" + elang.get(1).getTipe() + "),");
			}
			else if (elang.size() == 1){
				Level_1.add(elang.get(0).getNama() + "(" + elang.get(0).getPanjang() + "-" + elang.get(0).getTipe() + "),");
			}
			else {
				for(int i = elang.size()/3*2; i < elang.size(); i++){
					Level_3.add(elang.get(i).getNama() + "(" + elang.get(i).getPanjang() + "-" + elang.get(i).getTipe() + "),");
				}
				for(int i = elang.size()/3; i < elang.size()/3*2; i++){
					Level_2.add(elang.get(i).getNama() + "(" + elang.get(i).getPanjang() + "-" + elang.get(i).getTipe() + "),");
				}
				for (int i = 0; i < elang.size()/3; i++){
					Level_1.add(elang.get(i).getNama() + "(" + elang.get(i).getPanjang() + "-" + elang.get(i).getTipe() + "),");
				}
			}
			System.out.println("Location: " + kandang.getTipe());
			System.out.print("Level 3: ");
			for(String i:Level_3){
				System.out.print(i);
			}
			System.out.print("\nLevel 2: ");
			for(String i:Level_2){
				System.out.print(i);
			}
			System.out.print("\nLevel 1: ");
			for(String i:Level_1){
				System.out.print(i);
			}
			System.out.println("\n\nAfter rearrangment...");
			if(Level_2.size()==0 && Level_3.size()==0){
				System.out.print("Level 3: ");
				for(int i = Level_1.size()-1; i>= 0; i--){
					System.out.print(Level_1.get(i));
				}
				System.out.print("\nlevel 2: ");
				System.out.print("\nlevel 1: ");
			}
			else {
				System.out.print("level 3: ");
				for(int i=Level_2.size()-1; i >= 0; i--){
					System.out.print(Level_2.get(i));
				}
				System.out.print("\nlevel 2: ");
				for(int i=Level_1.size()-1; i >= 0; i--){
				System.out.print(Level_1.get(i));
				}
				System.out.print("\nlevel 1: ");
				for(int i=Level_3.size()-1; i >= 0; i--){
				System.out.print(Level_3.get(i));
				}
			}
			System.out.println();
		}
	}
}