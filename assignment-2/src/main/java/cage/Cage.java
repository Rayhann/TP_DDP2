package cage;
public class Cage{
	
	private String tipe;
	private String hewan;

	public String getHewan(){
		return hewan;
	}

	public String getTipe(){
		return tipe;
	}

	public Cage(String hewan){
		this.hewan = hewan;
		if(hewan.equals("parrot") || (hewan.equals("cat")) || (hewan.equals("hamster"))){
			this.tipe = "Indoor";
		}
		else {
			this.tipe = "Outdoor";
		}
	}
}