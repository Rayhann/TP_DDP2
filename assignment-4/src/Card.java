import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;

public class Card extends JButton {
    private int position;
    private ImageIcon front;
    private ImageIcon back;
    private boolean match = false;

    public Card(int value){
        try {
            this.position = value;

            // Front image before flip
            Image ima = ImageIO.read(new File("..\\image\\fifa.jpg"));
            Image image = ima.getScaledInstance(100, 100, Image.SCALE_SMOOTH);
            this.front = new ImageIcon(image);

            // Back image after flip
            Image pic = ImageIO.read(new File("..\\image\\" + value + ".jpg"));
            Image picture = pic.getScaledInstance(100, 100, Image.SCALE_SMOOTH);
            this.back = new ImageIcon(picture);

            this.setIcon(front);
        }
        catch (Exception e){
            System.out.println("Error, cannot find image");
        }
    }

    public int getPosition(){
        return this.position;
    }

    public void setMatch(boolean match) {
        this.match = match;
    }

    public boolean isMatch() {
        return this.match;
    }

    public ImageIcon getFront(){
        return this.front;
    }

    public ImageIcon getBack(){
        return this.back;
    }
}
