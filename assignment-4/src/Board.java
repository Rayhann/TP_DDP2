import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;


// Class to make boaard
public class Board extends JFrame {

    private List<Card> images;
    private Card selectedImage;
    private Card image1;
    private Card image2;
    private Timer t;
    private JPanel cardPanel; //To make JPanel
    private JLabel counter; // To make JPanel
    private int count;
    List<Card> imageList = new ArrayList<Card>(); // Arraylist to store Card
    List<Integer> imageVals = new ArrayList<Integer>(); // Arraylist to store id/position

    // Constructor
    public Board(){
        int pairs = 18;

        for (int i = 1; i <= pairs; i++){
            // Add twice because oen pairs
            imageVals.add(i);
            imageVals.add(i);
        }
        Collections.shuffle(imageVals); // Shuffle

        // Looping to make id into card
        for (int val : imageVals){
            Card gambar = new Card(val);
            gambar.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent AE){
                    selectedImage = gambar;
                    turn();
                }
            });
            imageList.add(gambar);
        }
        this.images = imageList;

        // set timer
        t = new Timer(800, new ActionListener() {
            public void actionPerformed(ActionEvent e){
                check();
            }
        });
        t.setRepeats(false);

        // set board
        Container papan = getContentPane();
        setLayout(new BorderLayout());

        cardPanel = new JPanel( new GridLayout(6,6));
        for(Card i : images) {
            cardPanel.add(i);
        }

        // Set Button
        JButton button1 = new JButton("Exit");
        JButton button2 = new JButton("Restart");
        JPanel buttonpanel = new JPanel();

        setTitle("Game of Memory");

        // Create exit button
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        // Create restart button
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (Card i : images){
                    i.setIcon(i.getFront());
                    i.setEnabled(true);
                    i.setMatch(false);
                }
                // After click restart button and make new game in the same window
                Collections.shuffle(imageVals);
                remove(cardPanel);
                revalidate();
                repaint();
                imageVals.clear();
                newGame();
                count = 0;
                counter.setText("Number of Tries: 0");
            }
        });

        // Add button to panel
        buttonpanel.add(button1);
        buttonpanel.add(button2);


        // Create number tries label
        counter = new JLabel("Number of Tries: 0");
        JPanel buttonpane2 = new JPanel(new FlowLayout());
        buttonpane2.add(counter);

        // Add all to Frame
        papan.add(cardPanel, BorderLayout.NORTH);
        papan.add(buttonpanel, BorderLayout.CENTER);
        papan.add(buttonpane2, BorderLayout.SOUTH);
    }

    // Create method for restart button
    private void newGame(){
        int pairs = 18;

        for (int i = 1; i <= pairs; i++){
            imageVals.add(i);
            imageVals.add(i);
        }

        Collections.shuffle(imageVals);
        imageList = new ArrayList<>();
        for (int val : imageVals){
            Card gambar = new Card(val);
            gambar.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent AE){
                    selectedImage = gambar;
                    turn();
                }
            });
            imageList.add(gambar);
        }
        this.images = imageList;

        // set timer
        t = new Timer(800, new ActionListener() {
            public void actionPerformed(ActionEvent e){
                check();
            }
        });
        t.setRepeats(false);


        cardPanel = new JPanel( new GridLayout(6,6));
        for(Card i : images) {
            cardPanel.add(i);
        }
        add(cardPanel,BorderLayout.NORTH);
        revalidate();
        repaint();

    }

    // Method to turn back the card
    public void turn(){
        if(image1 == null && image2 == null){
            image1 = selectedImage;
            image1.setIcon(image1.getBack());
        }
        if(image1 != null && image1 != selectedImage && image2 == null){
            image2 = selectedImage;
            image2.setIcon(image2.getBack());
            t.start();
            count++;
            counter.setText("Number of Tries: " + count);
        }
    }

    // Method to check if the card is pairs or not
    public void check(){
        if(image1.getPosition() == image2.getPosition()){ // if match
            image1.setEnabled(false);
            image2.setEnabled(false);
            image1.setMatch(true);
            image2.setMatch(true);
            // if all card is open
            if(this.gameWon()){
                JOptionPane.showMessageDialog(this, "Yeay, congratulations!!");
                System.exit(0);
            }

        }
        else {
            // Back to the first condition
            image1.setIcon(image1.getFront());
            image2.setIcon(image2.getFront());
        }
        // Reset card if not match
        image1 = null;
        image2 = null;
    }

    // When the game won
    public boolean gameWon(){
        for(Card i : this.images){
            if(i.isMatch() == false){
                return false;
            }
        }
        return true;
    }
}
