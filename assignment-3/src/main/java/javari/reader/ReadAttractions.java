package javari.reader;

import javari.park.SelectedAttraction;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class ReadAttractions extends CsvReader {

    List<SelectedAttraction> attractionsList = new ArrayList<>();
    boolean cofValid = true;
    boolean daValid = true;
    boolean cmValid = true;
    boolean pcValid = true;

    public ReadAttractions(Path file) throws IOException{
        super(file);
        processData();
    }

    @Override
    public void processData() {
        List<String> attractionLines = getLines();

        for (String lines : attractionLines) {
            String[] data = lines.split(",");
            switch (data[1]){
                case "Circle of Fires" : if(!"LionWhaleEagle".contains(data[0])){
                    cofValid = false;
                } break;

                case "Dancing Animals" : if(!"ParrotSnakeCatHamster".contains(data[0])){
                    daValid = false;
                } break;

                case "Counting Masters" : if(!"HamsterWhaleParrot".contains(data[0])){
                    cmValid = false;
                } break;

                case "Passionate Coders" : if(!"HamsterCatSnake".contains(data[0])){
                    pcValid = false;
                } break;
            }
        }
    }


    @Override
    public long countInvalidRecords() {

        return 4-countValidRecords();
    }

    @Override
    public long countValidRecords() {
        int cntValid=0;
        if(cofValid) cntValid++;
        if(daValid) cntValid++;
        if(cmValid) cntValid++;
        if(pcValid) cntValid++;

        return cntValid;
    }
}
