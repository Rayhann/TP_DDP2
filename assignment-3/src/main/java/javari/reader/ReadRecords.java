package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class ReadRecords extends CsvReader {

    List<String> validSpecies;
    List<String> validRecord = new ArrayList<>();
    public ReadRecords(Path file)throws IOException{
        super(file);
    }

    public List<String> getValidRecord() {
        return validRecord;
    }

    public void setValidSpecies(List<String> validSpesies) {
        this.validSpecies = validSpesies;
    }

    @Override
    public void processData() {
    }

    @Override
    public long countInvalidRecords() {
        return getLines().size()-countValidRecords();
    }

    @Override
    public long countValidRecords() {
        List<String> recordLines = getLines();
        int cntValid=0;
        for(String line : recordLines){
//            String[] data = line.split(",");
//            int ID = Integer.parseInt(data[0]);
//            String species = data[1];
//            String name = data[2];
//            String gender = data[3];
//            double length = Double.parseDouble(data[4]);
//            double weight = Double.parseDouble(data[5]);
//            String specialStatus = data[6];
//            String condition = data[7];
//            if(!validData(species)) continue;
//            //todo doing something kalo aves pregnant dan sebaliknya
            cntValid++;
            //System.out.println("nani");
            validRecord.add(line);
        }
        return cntValid;
    }

    public boolean validData(String species){
        for(String specieslines : this.validSpecies){
            if(specieslines.split(",")[1].equals(species)) return true;
        }
        return false;
    }

}
