package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class ReadCategories extends CsvReader {
    private List<String> validSection = new ArrayList<>();
    private List<String> validSpecies = new ArrayList<>();
    private List<String> validCategories = new ArrayList<>();
    boolean mammalValid = true;
    boolean reptilesValid = true;
    boolean avesValid = true;
    public ReadCategories(Path file) throws IOException {
        super(file);
        processData();
    }

    @Override
    public void processData() {
        List<String> categoriesLines = getLines();
        boolean mammalSectionValid = true;
        boolean reptilesSectionValid = true;
        boolean avesSectionValid = true;

        for (String lines : categoriesLines){
            String[] data = lines.split(",");
            switch (data[2]){
                case "Explore the Mammals" : if(!data[1].equals("mammals")|!validData("mammals",data[0])) {
                    mammalSectionValid = false;
                    mammalValid = false;
                } else validSpecies.add("mammals,"+data[0]); break;

                case "Reptilian Kingdom" : if(!data[1].equals("reptiles")|!validData("reptiles",data[0])) {
                    reptilesSectionValid = false;
                    reptilesValid = false;
                } else validSpecies.add("reptiles,"+data[0]); break;

                case "World of Aves" : if(!data[1].equals("aves")|!validData("aves",data[0])) {
                    avesSectionValid = false;
                    avesValid = false;
                } else validSpecies.add("aves,"+data[0]); break;
            }
        }

        if(mammalValid) validSection.add("Explore the Mammals");
        if(avesValid) validSection.add("World of Aves");
        if(reptilesValid) validSection.add("Reptilian Kingdom");
    }

    private boolean validData(String kingdom,String species){
        switch (kingdom){
            case "mammals" : if("LionHamsterCatWhale".contains(species)) return true;
            case "reptiles" : if(species.equals("Snake")) return true;
            case "aves" : if("ParrotEagle".contains(species)) return  true;
            default: return false;
        }
    }

    @Override
    public long countInvalidRecords() {
        List<String> categoriesLines = getLines();
        int cntInvalid=0;
        for (String lines : categoriesLines){
            String[] data = lines.split(",");
            switch (data[2]){
                case "Explore the Mammals" : if(!data[1].equals("mammals")|!validData("mammals",data[0])) {
                    mammalValid = false;
                }  break;
                case "Reptilian Kingdom" : if(!data[1].equals("reptiles")|!validData("reptiles",data[0])) {
                    reptilesValid = false;
                } break;
                case "World of Aves" : if(!data[1].equals("aves")|!validData("aves",data[0])) {
                    avesValid = false;
                } break;
            }
        }

        if(!mammalValid) cntInvalid++;
        if(!reptilesValid) cntInvalid++;
        if(!avesValid) cntInvalid++;

        return cntInvalid;
    }

    @Override
    public long countValidRecords() {
        return 3-countInvalidRecords();
    }

    public List<String> getValidSection() {
        return validSection;
    }

    public List<String> getValidSpecies() {
        return validSpecies;
    }
}
