package javari;
import javari.animal.*;
import javari.park.*;
import javari.reader.ReadAttractions;
import javari.reader.ReadCategories;
import javari.reader.ReadRecords;
import javari.writer.RegistrationWriter;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class A3Festival {
    private static Scanner input = new Scanner(System.in);
    private static String CSV_PATH = "C:\\Users\\RAYHAN\\Documents\\Tugas Pemograman\\TP_DDP2\\assignment-3\\data";
    private static ReadAttractions Att;
    private static ReadCategories Cate;
    private static ReadRecords Rec;
    private static List<Section> sections = new ArrayList<>();
    private static List<Attraction> attractions = new ArrayList<>();
    private static List<Animal> animalList = new ArrayList<>();
    private static List<Visitor> visitorList = new ArrayList<>();

    public static boolean createReaderCsv(String path){
        try {
            Att = new ReadAttractions(Paths.get(path+"\\animals_attractions.csv"));
            Cate = new ReadCategories(Paths.get(path+"\\animals_categories.csv"));
            Rec = new ReadRecords(Paths.get(path+"\\animals_records.csv"));
        } catch (IOException e){
            System.out.println("... Opening default section database from data. ... File not found or incorrect file!");
            return false;
        }
        Rec.setValidSpecies(Cate.getValidSpecies()); 
        createSection(Cate);
        createAnimal(Rec);
        createAttraction(Att);
        inputAttractionToAnimal();
        System.out.println();
        return true;
    }

    public static void createSection(ReadCategories Cate){
        List<String> validSection = Cate.getValidSection();
        List<String> validSpecies = Cate.getValidSpecies();
        List<String> validMammals = new ArrayList<>();
        List<String> validReptiles = new ArrayList<>();
        List<String> validAves = new ArrayList<>();

        for(String speciesLines : validSpecies){
            String[]data = speciesLines.split(",");
            switch (data[0]){
                case "mammals" : validMammals.add(data[1]); break;
                case "reptiles" : validReptiles.add(data[1]); break;
                case "aves" : validAves.add(data[1]); break;
            }
        }

        for(String sectionLines : validSection){
            switch (sectionLines) {
                case "Explore the Mammals" : sections.add(new Section(sectionLines,"mammals",validMammals)); break;
                case "World of Aves" : sections.add(new Section(sectionLines,"aves",validAves)); break;
                case "Reptilian Kingdom": sections.add(new Section(sectionLines,"reptiles",validReptiles)); break;
            }
        }
    }

    public static void createAnimal(ReadRecords Rec){
        List<String> animalLines = Rec.getLines();
        for(String animal : animalLines){
            Animal newAnimal=null;
            String[] data = animal.split(",");
            int ID = Integer.parseInt(data[0]);
            String species = data[1];
            String name = data[2];
            Gender gender = Gender.parseGender(data[3]);
            double length = Double.parseDouble(data[4]);
            double weight = Double.parseDouble(data[5]);
            String specialStatus = data[6];
            Condition condition = Condition.parseCondition(data[7]);

            if ("LionHamsterCatWhale".contains(species)){
                newAnimal = new Mammals(ID,species,name,gender,length,weight,condition);
                newAnimal.setSpecialStatus(specialStatus);
            }
            if("Snake".contains(species)){
                newAnimal = new Reptiles(ID,species,name,gender,length,weight,condition);
                newAnimal.setSpecialStatus(specialStatus);
            }
            if("ParrotEagle".contains(species)){
                newAnimal = new Aves(ID,species,name,gender,length,weight,condition);
                newAnimal.setSpecialStatus(specialStatus);
            }
            animalList.add(newAnimal);
        }

    }

    public static void createAttraction(ReadAttractions Att){
        List<String> attractionsLines = Att.getLines();
        for(String attractionline : attractionsLines){
            String[] data = attractionline.split(",");
            String type = data[0];
            String name = data[1];
            List<Animal> performer = null;
            switch (type){
                case "Whale" : performer = findAllAnimal(type); break;
                case "Lion" : performer = findAllAnimal(type); break;
                case "Eagle" : performer = findAllAnimal(type); break;
                case "Cat" : performer = findAllAnimal(type); break;
                case "Parrot" : performer = findAllAnimal(type); break;
                case "Hamster" : performer = findAllAnimal(type); break;
                case "Snake" : performer = findAllAnimal(type); break;
            }
            Attraction attraction = new Attraction(name,type,performer);
            attractions.add(attraction);
        }

    }

    public static List<Animal> findAllAnimal(String type){
        List<Animal> out = new ArrayList<>();
        for (Animal animal : animalList){
            if(animal.getType().equalsIgnoreCase(type)) out.add(animal);
        }
        return out;
    }

    public static void inputAttractionToAnimal(){
        for(Attraction attraction : attractions){
            String type = attraction.getType();
            List<Animal> performer = null;
            switch (type){
                case "Whale" : performer = findAllAnimal(type); break;
                case "Lion" : performer = findAllAnimal(type); break;
                case "Eagle" : performer = findAllAnimal(type); break;
                case "Cat" : performer = findAllAnimal(type); break;
                case "Parrot" : performer = findAllAnimal(type); break;
                case "Hamster" : performer = findAllAnimal(type); break;
                case "Snake" : performer = findAllAnimal(type); break;
            }
            for(Animal animal : performer){
                animal.putAttraction(attraction);
            }
        }

    }

    public static void registrationActivity() throws IOException{
        List<String> validSection = Cate.getValidSection();
        if (validSection == null) {
            System.out.println("\nJavari Park has 0 valid Sections, please come again later");
            return;
        }
        System.out.printf("\nJavari Park has %d sections:\n" ,validSection.size());
        int cnt = 1;
        for(String section : validSection){
            System.out.println(cnt+". "+section);
            cnt++;
        }
        System.out.print("Please choose your preferred section (type the number): ");
        int cmd = Integer.parseInt(input.nextLine());
        Section section = sections.get(cmd-1);
        sectionActivity(section);
    }

    public static void sectionActivity(Section section) throws IOException{
        System.out.printf("\n--%s--\n",section.getName());
        System.out.println(section.getInfo());
        System.out.print("Please choose your preferred animals (type the number): ");
        String inputs = input.nextLine();
        if(inputs.equalsIgnoreCase("#")) registrationActivity();
        int cmd = Integer.parseInt(inputs);
        String species = section.getSpesiesList().get(cmd-1);
        Animal animal = findAnimal(species);
        animalActivity(animal,section);
    }

    public static Animal findAnimal(String species){
        for(Animal animals : animalList){
            if(animals.getType().equalsIgnoreCase(species)) return animals;
        }
        return null;
    }

    public static void animalActivity(Animal animal,Section section) throws IOException{
        System.out.printf("\n--%s--\n",animal.getType());
        List<Attraction> animalAttraction = animal.getAttractionList();
        int cnt=1;
        for(Attraction attraction : animalAttraction){
            System.out.println(cnt+" "+attraction.getName());
            cnt++;
        }
        System.out.print("Please choose your preferred attractions (type the number): ");
        String inputs = input.nextLine();
        if(inputs.equalsIgnoreCase("#")) sectionActivity(section);
        int cmd = Integer.parseInt(inputs);
        Attraction attraction = animal.getAttractionList().get(cmd-1);
        visitorRegistration(attraction);
    }

    public static void visitorRegistration(Attraction attraction) throws IOException{

        System.out.println("Wow, one more step,");
        System.out.print("please let us know your name: ");
        String name = input.nextLine();
        Visitor visitor = findVisitor(name);
        if (visitor == null)  {
            visitor =  new Visitor(name);
            visitorList.add(visitor);
        }
        visitor.addSelectedAttraction(attraction);
        System.out.println("Yay, final check!");
        System.out.println("Here is your data, and the attraction you choose:");
        System.out.println("name: "+visitor.getVisitorName());
        for(SelectedAttraction show : visitor.getSelectedAttractions()){
            System.out.println("Attractions: "+show.getName()+" -> "+show.getType());
            System.out.print("with: ");
            for(Animal performer : show.getPerformers()){
                System.out.print(performer.getName()+", ");
            }
            System.out.println();
        }

        System.out.print("Is the data correct? (Y/N):");
        String confirm = input.nextLine();
        if(confirm.equalsIgnoreCase("N")) visitorRegistration(attraction);
        System.out.print("Thank you for your interest. Would you like to register to other attractions? (Y/N): ");
        confirm = input.nextLine();
        RegistrationWriter apalah = new RegistrationWriter();
        if(confirm.equalsIgnoreCase("y")){
            apalah.writeJson(visitor, Paths.get("C:\\Users\\RAYHAN\\Documents\\Tugas Pemograman\\TP_DDP2\\assignment-3\\src\\main\\java\\javari\\writer"));
            registrationActivity();
        }
        else {
            apalah.writeJson(visitor, Paths.get("C:\\Users\\RAYHAN\\Documents\\Tugas Pemograman\\TP_DDP2\\assignment-3\\src\\main\\java\\javari\\writer"));
            System.out.println();
            System.exit(0);
        }
    }

    public static Visitor findVisitor(String name){
        for(Visitor visitor : visitorList){
            if(visitor.getVisitorName().equalsIgnoreCase(name)){
                return visitor;
            }
        }
        return null;
    }

    public static void main(String[] args) throws IOException{
        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        System.out.println();

        while(!createReaderCsv(CSV_PATH)){
            System.out.print("Please provide the source data path: ");
            CSV_PATH = input.nextLine();
        }

        System.out.println("... Loading... Success... System is populating data...");
        System.out.println();

        System.out.printf("Found %d valid sections and %d invalid sections\n",
                Cate.countValidRecords(),Cate.countInvalidRecords());
        System.out.printf("Found %d valid attractions and %d invalid attractions\n",
                Att.countValidRecords(),Att.countInvalidRecords());
        System.out.printf("Found %d valid animal categories and %d invalid animal categories\n",
                Cate.countValidRecords(),Cate.countInvalidRecords());
        System.out.printf("Found %d valid animal records and %d invalid animal records\n\n",
                Rec.countValidRecords(),Rec.countInvalidRecords());


        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        System.out.println();
        System.out.println("Please answer the questions by typing the number. Type # if you want to return to the previous menu");
        registrationActivity();
    }
}