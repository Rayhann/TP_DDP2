package javari.animal;

public class Aves extends Animal {
    public Aves(Integer id, String type, String name, Gender gender, double length, double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
    }

    @Override
    protected boolean specificCondition() {
        if(!getSpecialStatus().equals("laying eggs")) return true;
        return false;
    }
}
