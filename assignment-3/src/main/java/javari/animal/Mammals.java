package javari.animal;

public class Mammals extends Animal {

    public Mammals(Integer id, String type, String name, Gender gender, double length, double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
    }


    @Override
    protected boolean specificCondition() {
        boolean condition = true;
        if (getType().equals("lion") & getGender() == Gender.FEMALE ){
            condition = false;
        }

        if(getSpecialStatus().equals("pregnant")) {
            condition = false;
        }
        return condition;
    }
}
