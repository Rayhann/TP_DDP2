package javari.animal;

public class Reptiles extends Animal {

    public Reptiles(Integer id, String type, String name, Gender gender, double length, double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
    }

    @Override
    protected boolean specificCondition() {
        if(getSpecialStatus().equals("tame")) return true;
        return false;
    }
}
