package javari.park;

import javari.animal.Animal;

import java.util.ArrayList;
import java.util.List;

public class Attraction implements SelectedAttraction {

    private String name;
    private String type;
    private List<Animal> performers = new ArrayList<>();
    private boolean valid;

    public Attraction(String name, String type, List<Animal> performers) {
        this.name = name;
        this.type = type;
        this.performers = performers;
    }

    @Override
    public boolean addPerformer(Animal performer) {
        return false;
    }

    @Override
    public String getName() {
        return this.name;
    }

    public boolean isValid() {
        return valid;
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public List<Animal> getPerformers() {
        return this.performers;
    }
}
