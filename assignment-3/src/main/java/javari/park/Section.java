package javari.park;

import javari.animal.Animal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Section {
    private String name;
    private String kingdom;
    //private List<Animal> member;
    private List<String> spesiesList;

    public Section(String name, String kingdom,List<String> spesiesList) {
        this.name = name;
        this.kingdom = kingdom;
        this.spesiesList = spesiesList;
    }

    public List<String> getSpesiesList() {
        return spesiesList;
    }

    public String getInfo(){
        String out = "";
        int cnt=1;
        for (String species : spesiesList){
            out+=cnt+". "+species+"\n";
            cnt++;
        }
        return out;
    }

    public String getName() {
        return name;
    }

    public List<Animal> getMember() {
        return null;
    }

    @Override
    public String toString() {
        return name;
    }
}
