package javari.park;

import java.util.ArrayList;
import java.util.List;

public class Visitor implements Registration {

    private String name;
    private int ID = 0;
    private List<SelectedAttraction> selectedAttractions = new ArrayList<>();

    public Visitor(String name) {
        this.name = name;
    }



    @Override
    public int getRegistrationId() {
        return this.ID;
    }

    @Override
    public String getVisitorName() {
        return this.name;
    }

    @Override
    public void setVisitorName(String name) {
        this.name = name;
    }

    @Override
    public List<SelectedAttraction> getSelectedAttractions() {
        return this.selectedAttractions;
    }

    @Override
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        this.selectedAttractions.add(selected);
        return true;
    }
}
